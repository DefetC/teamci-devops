# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# This script is used to install dependencies needed for deployment
# It does not deploy the system

# You need ansible version 2.8 to run this script

export ANSIBLE_HOST_KEY_CHECKING=FALSE
ansible-playbook -i inventory site.yml