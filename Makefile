# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

help:
	@echo "  build-frontend     Builds and dockerize frontend"
	@echo "  build-webapi       Build frontend and backend-webapi docker image"
	@echo "  build-algo         Build backend-algo docker image"
	@echo "  build              Build whole system"
	@echo "  up                 Start teamic system. Must be called after system build"
	@echo "  down               Stop teamic system"

build-frontend:
	@echo "Building Project"
	@cd ../swen90013-team-ic-front-end ; \
	if [ -d "sites" ]; then rm -Rf sites; fi
	@cd ../swen90013-team-ic-front-end/authorized-site ; \
	npm install; ng build --prod
	@echo "Building Docker image teamic-frontend"
	@cd ../swen90013-team-ic-front-end ; \
	docker build -t teamic-frontend .

build-webapi:	
	@echo "Reading AWS Configurations"
	@if [ -f "aws.config" ]; then :; else echo "File not found. Build failed"; exit 1; fi	
	@echo "Building Docker image teamic-backend-webapi"
	cp aws.config ../teamic-backend-webapi/aws.config
	@cd ../teamic-backend-webapi ; \
	docker build -t teamic-backend-webapi --build-arg FE_IMAGE=teamic-frontend . ; \
	rm aws.config

build-algo:
	@echo "Reading AWS Configurations"
	@if [ -f "aws.config" ]; then :; else echo "File not found. Build failed"; exit 1; fi
	@echo "Building Docker image teamic-backend-algo"
	@cp aws.config ../teamic-backend-algo/aws.config
	@cd ../teamic-backend-algo ; \
	docker build -t teamic-backend-algo . ; \
	rm aws.config

build: build-frontend build-webapi build-algo

up:
	@echo "Reading Algo configurations"
	@if [ -f "algo.config" ]; then :; else echo "File algo.config not found. Failed to start system"; exit 1; fi
	@cp algo.config ../teamic-backend-algo/deploy/.env
	@echo "Creating ic_network"
	@docker network create --driver bridge ic_network || true
	@echo "Starting Algo"
	@cd ../teamic-backend-algo/deploy ; \
	docker-compose -p algo up -d

	@echo "Reading Webapi configurations"
	@if [ -f "webapi.config" ]; then :; else echo "File webapi.config not found. Failed to start system"; exit 1; fi
	@cp webapi.config ../teamic-backend-webapi/deploy/.env
	@echo "Starting Webapi"
	@cd ../teamic-backend-webapi/deploy ; \
	docker-compose -p webapi up -d
	

down:
	@echo "Reading Algo configurations"
	@if [ -f "algo.config" ]; then :; else echo "File algo.config not found. Failed to start system"; exit 1; fi
	@cp webapi.config ../teamic-backend-webapi/deploy/.env
	@echo "Shuting down webapi"	
	@cd ../teamic-backend-webapi/deploy ; \
	docker-compose -p webapi down ; \
	rm .env
	@cp algo.config ../teamic-backend-algo/deploy/.env
	@echo "Shuting down Algo"
	@cd ../teamic-backend-algo/deploy ; \
	docker-compose -p algo down ; \
	rm .env
	

	
	